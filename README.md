# Juneway less 3.4

### Описание задания

Добавить в шаблон с прошлого задания Апстримы которые беруться из переменных  
upstream:  
upstream1:  
server: "127.0.0.1:8002"  
upstream2:  
server: "127.0.0.1:8003"  
upstream3:  
server: "127.0.0.2:8004"  
в конфиге должно быть так  
upstream test {  
server 127.0.0.1:8002;  
server 127.0.0.1:8003;  
server 127.0.0.2:8004;  
}  
1. Они должны попасть на оба сервера.  
Сервер нэйм разные ,апстримы одинаковые  
2. При добавлении нового апстрима я должен деплоить не меняя сам Шаблон конфига нжингса.  

На двух серверах должно быть по 2 конфига test1.... test2....  




### Решение


https://gitlab.com/maxchinyonov/lesson3_4
https://gitlab.com/maxchinyonov/lesson3_4/-/blob/master/roles/nginx/templates/default.conf.j2


upstream test {
{% for item in upstream.values() %}
server {{ item.server }};
{% endfor %}
}




У нас имеется upstream со значениями 


upstream1', 'value': {'server': '127.0.0.1:8002'}
upstream2...
и т.д


Т.е как мы видим значения каждого апстрима будет - {'server': '127.0.0.1:8002'} ...


Мы определяем это value как переменную "item" и затем выводим все значения для данного "item" для аттрибута server c помощью цикла


Таким образом получаем отдельно список ip адресов построчно.
Затем добавляем в начале строки "server" и к концу строки  " ; "
